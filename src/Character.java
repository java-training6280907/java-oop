public class Character {
    private Weapon weapon;
    private int health;
    private final String name;

    public Character(String name, int health, Weapon weapon) {
        this.name = name;
        this.health = health;
        this.weapon = weapon;
    }

    public Character(String name, int health) {
        this.name = name;
        this.health = health;
    }

    public String getName() {
        return name;
    }

    public boolean hasWeapon() {
        return this.weapon != null;
    }

    public void fight(Character character) {
        System.out.println(this.name + " wants to fight...");

        if (character.hasWeapon()) {
            increaseHealth();

            int damage = this.weapon.makeHit();

            System.out.println(character.getName() + " looses " + damage + " health points");

            character.takeDamage(damage);
        } else {
            System.out.println(character.getName() + " has no weapon and running away...");
        }
    }

    public void takeDamage(int damage) {
        this.health -= damage;

        System.out.println(this.name + " remaining health is " + this.health);
    }

    private void increaseHealth() {
        System.out.println(this.name + " uses special skill and increases health in 10 points");

        this.health += 10;
    }
}
