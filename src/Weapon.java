public class Weapon {
    private final String name;
    private final int hitPoints;

    public Weapon(String name, int hitPoints) {
        this.name = name;
        this.hitPoints = hitPoints;
    }

    public int makeHit() {
        System.out.println("Makes a hit with " + this.name);

        return this.hitPoints;
    }
}
