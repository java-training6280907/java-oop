public class Main {
    public static void main(String[] args) {
        Weapon bow = new Weapon("BowAndArrows", 15);
        Weapon sword = new Weapon("Sword", 20);

        Character elf = new Character("Elf", 100, bow);
        Character knight = new Character("Knight", 80, sword);

        elf.fight(knight);
    }
}
